﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace TextToSpeech_Basics
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }

        private void ConvertButton_Clicked(object sender, EventArgs e)
        {
            TextToSpeech.SpeakAsync(TextController.Text);
        }
    }
}
