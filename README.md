## Text To Speech
Simple use case of Text To speech in Xamarin Forms

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-texttospeech/-/raw/master/img1.png" width="200" height=400" />
</p>

## Import Library
```bash
using Xamarin.Essentials;
```
